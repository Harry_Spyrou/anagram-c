﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        private static bool AnagramResolver(string firstString, string secondString)
        {
            string[] firstStringArray = firstString.Split(new[] {' '});
            string joinedString = string.Join("", firstStringArray); ;

            return joinedString.OrderBy(c => c).SequenceEqual(secondString.OrderBy(c => c));
        }
        static void Main(string[] args)
        {
            string a = "mama gran";
            string b = "anagram";
            //AnagramResolver(a, b);

            Console.WriteLine(AnagramResolver(a, b));

            string c = "marg ana";
            string d = "anagram";
            Console.WriteLine(AnagramResolver(c, d));
        }
    }
}
